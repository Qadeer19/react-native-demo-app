import React,  { Component } from 'react';
import { Text, View, TextInput, StyleSheet, Button, Image, TouchableHighlight  } from 'react-native';
import HeaderComponent from './HeaderComponent';

const backgroundColor = '#007256';
export default class Login extends Component {
    constructor() {
        super()
    }
    static navigationOptions = ({ navigation }) => {
        let drawerLabel = 'Login';
        let drawerIcon = () => (
            <Image
                source={require('../../icons/cloud-icon.png')}
                style={{ width: 26, height: 26, tintColor: backgroundColor }}
            />
        );
        return { drawerLabel, drawerIcon };
    }
    render() {
        return (
            <View style={styles.container}>
                <HeaderComponent {...this.props} />
                <TouchableHighlight style={{ 
                                            margin: 20, 
                                            width: 200, 
                                            height: 45,
                                            backgroundColor: 'darkviolet',
                                            padding: 10,
                                            alignItems: 'center',
                                         }}
                    onPress={() => {
                        this.props.navigation.goBack();                        
                    }}>
                    <Text style={{color: 'white', fontSize: 18}}>Back to Home</Text>
                </TouchableHighlight>
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: backgroundColor,
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
  });