import React, {Component} from 'react';
import {PropTypes} from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from "react-native";
import { Actions } from 'react-native-router-flux';

class NavDrawerPanel extends Component {

    render() {
        return (
            <View style={styles.container}>
                <TouchableHighlight onPress={ () => { Actions.home() } }>
                    <Text>Home</Text>
                </TouchableHighlight>
                <TouchableHighlight  onPress={ () => { Actions.about() } }>
                    <Text>About</Text>
                </TouchableHighlight>
                <TouchableHighlight>
                    <Text>Friends</Text>
                </TouchableHighlight>
            </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    backgroundColor: 'grey'
  },
})

export default NavDrawerPanel;