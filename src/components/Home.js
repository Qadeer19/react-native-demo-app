import React, {Component} from 'react'
import { TouchableOpacity, Text, View, Button, StyleSheet, Dimensions, Alert, Image } from 'react-native';
import HeaderComponent from './HeaderComponent';
import { About } from '../../screenNames'

var { height } = Dimensions.get('window');
 
var box_count = 3;
var box_height = height / box_count;
const backgroundColor = '#0067a7';

class Home extends Component {
    constructor(props) {
      super(props)
      this.state = { count: 0 }
    }

    static navigationOptions = ({ navigation }) => {
        let drawerLabel = 'Home';
        let drawerIcon = () => (
            <Image
                source={require('../../icons/home-icon.png')}
                style={{ width: 26, height: 26, tintColor: backgroundColor }}
            />
        );
        return {drawerLabel, drawerIcon};
    }

    add = () => {
        if(this.state.count<5) {
            this.setState({
                count: this.state.count+1
            })
        } 
    }
    subs = () => {
        if(this.state.count>0) {
            this.setState({
                count: this.state.count-1
            })
        } else {
            Alert.alert(
                'Negative Numbers',
                'You cannot substrate more',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
            )
        }
    }
    render() {
    return (
            <View style={styles.container}>
            
                <HeaderComponent {...this.props} />

                <View style={[styles.box, styles.box1]}>
                    <Text style={[styles.countText]}>
                        { this.state.count }
                    </Text>
                </View>
                <View style={[styles.box, styles.box2]}>
                    <TouchableOpacity style={styles.button} onPress={this.subs}>
                            <Text style={{ fontSize: 50}}>-</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this.add}>
                        <Text style={{ fontSize: 50}}>+</Text>
                    </TouchableOpacity>
                    </View>
                <View style={[styles.box, styles.box3]}>
                    <TouchableOpacity style={styles.button} onPress={() => {
                                            const { navigate } = this.props.navigation;
                                            navigate(About);                                             
                                        }}>
                        <Text style={{ fontSize: 10}}>About Us</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
        container: {
            flex: 1,
            flexDirection: 'column'
        },
        box: {
            height: box_height,
            alignItems:'center',
            justifyContent:'center',
        },
        box1: {
            backgroundColor: '#2196F3',
        },
        box2: {            
            flexDirection: 'row',
            backgroundColor: '#8BC34A',
            justifyContent: 'space-between', 
        },
        box3: {
            backgroundColor: '#e3aa1a'
        },
        button: {
            alignItems: 'center',
            backgroundColor: '#DDDDDD',
            padding: 10,
            width: 150
        },
        countText: {
            color: '#FF00FF',
            fontSize: 300
        }
});

export default Home