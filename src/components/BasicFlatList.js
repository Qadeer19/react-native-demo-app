import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View, Image, TouchableHighlight, Alert } from 'react-native';
import flatListData from '../../data/flatListData';
import HeaderComponent from './HeaderComponent';

const backgroundColor = '#0067a7';
class FlatListItem extends Component {
    render() {          
        return (
          <View style={{
                flex: 1,
                backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen': 'tomato'                
          }}>   
            <View style={{
                marginBottom: 5,
                flexDirection: 'row',
            }}>         
                    <Image 
                        source={{uri: this.props.item.imageUrl}}
                        style={{width: 100, height: 100, margin: 5}}>

                    </Image>
                    <View style={{
                        flexDirection: 'column',
                    }}>
                        <Text style={styles.flatListItem}>{this.props.item.name}</Text>
                        <Text style={styles.flatListItem}>{this.props.item.foodDescription}</Text>
                    </View>
                    <TouchableHighlight style={{ 
                                                marginRight: 0, 
                                                width: 100, 
                                                height: 45,
                                                backgroundColor: 'darkviolet',
                                                padding: 10,
                                                alignItems: 'center',
                                            }}
                        onPress={() => {
                            Alert.alert(
                                this.props.item.name,
                                this.props.item.foodDescription,
                                [
                                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                                ],
                                { cancelable: false }
                            )
                        }}>
                        <Text style={{color: 'white', fontSize: 18}}>Detail</Text>
                    </TouchableHighlight>
            </View>        
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between'}}>
                
                </View>    
          </View>
        );
    }
}
const styles = StyleSheet.create({
    flatListItem: {
        color: 'white',
        padding: 10,
        fontSize: 16,  
    }
});

export default class BasicFlatList extends Component {
    
    static navigationOptions = ({ navigation }) => {
        let drawerLabel = 'Listing';
        let drawerIcon = () => (
            <Image
                source={require('../../icons/cloud-icon.png')}
                style={{ width: 26, height: 26, tintColor: backgroundColor }}
            />
        );
        return {drawerLabel, drawerIcon};
    }

    render() {
      return (
        <View style={{flex: 1, marginTop: 22}}>
            <HeaderComponent {...this.props} />
            <FlatList 
                data={flatListData}
                renderItem={({item, index})=>{
                    //console.log(`Item = ${JSON.stringify(item)}, index = ${index}`);
                    return (
                    <FlatListItem item={item} index={index}>

                    </FlatListItem>);
                }}
                >

            </FlatList>
        </View>
      );
    }
}