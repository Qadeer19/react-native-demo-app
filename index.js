/** @format */

import {AppRegistry, Dimensions} from 'react-native';
import { DrawerNavigator } from 'react-navigation';
// import App from './App';
import {name as appName} from './app.json';

//Components
import HomeComponent from './src/components/Home';
import AboutComponent from './src/components/About';
import LoginComponent from './src/components/Login';
import BasicFlatListComponent from './src/components/BasicFlatList';

//Screen names
import { Home, About, Login, FlatList } from './screenNames';
//Screen size
var {height, width} = Dimensions.get('window');

let routeConfigs = {
    Home: {
        path: '/',
        screen: HomeComponent,
    },
    FlatList: {
        path: '/flatlist',
        screen: BasicFlatListComponent,
    },
    About: {
        path: '/about',
        screen: AboutComponent,
    },
    Login: {
        path: '/login',
        screen: LoginComponent,
    }
};
let drawerNavigatorConfig = {    
    initialRouteName: Home,
    drawerWidth: width / 2,
    drawerPosition: 'left',
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',  
    // drawerBackgroundColor: 'orange',
    contentOptions: {
        activeTintColor: 'red',
    },
    order: [Home,FlatList,About,Login]
};
const App = DrawerNavigator(routeConfigs, drawerNavigatorConfig);

AppRegistry.registerComponent(appName, () => App);
